package Control;

import Model.BankAccount;

public class Controller {
	private BankAccount bank;
	private int notWithdraw;
	
	public Controller(){
		bank = new BankAccount(); 
	}
	
	public void deposit(String p){
		bank.deposit(p);
	}
	
	public void withdraw(String p){
		double a = Double.parseDouble(p);
		double b = Double.parseDouble(bank.getBalance());
//		System.out.println(a);
//		System.out.println(b);
		if (b < a){
			notWithdraw = 1;
		}
		else{
			notWithdraw = 0;
			bank.withdraw(p);
		}
	}
	
	public String getBalance(){
		return bank.getBalance();
	}
	
	public int getValue(){
		return notWithdraw;
	}
}
