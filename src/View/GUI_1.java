package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI_1 {
	private JFrame frame;
	private JPanel colorPanel;
	private JPanel colorPanelMain;
	private JButton red;
	private JButton green;
	private JButton blue;
	
	public GUI_1(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new BorderLayout());
		colorPanel.setBackground(new Color(255,255,255));
		
		colorPanelMain = new JPanel();
		colorPanelMain.setLayout(new FlowLayout());
		colorPanelMain.setBackground(new Color(255,255,255));
		
		red = new JButton("Red");
		red.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				colorPanel.setBackground(new Color(255,0,4));	
				colorPanelMain.setBackground(new Color(255,0,4));	
			}
		});
		
		green = new JButton("Green");
		green.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				colorPanel.setBackground(new Color(62,255,36));	
				colorPanelMain.setBackground(new Color(62,255,36));
			}
		});
		
		blue = new JButton("Blue");
		blue.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				colorPanel.setBackground(new Color(36,255,245));	
				colorPanelMain.setBackground(new Color(36,255,245));
			}
		});
		
		colorPanelMain.add(red);
		colorPanelMain.add(green);
		colorPanelMain.add(blue);
		colorPanel.add(colorPanelMain, BorderLayout.SOUTH);
		frame.add(colorPanel, BorderLayout.CENTER);	
		
		frame.setVisible(true);
	}
}
