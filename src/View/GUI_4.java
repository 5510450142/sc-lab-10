package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI_4 {
	private JFrame frame;
	private JPanel colorPanel;
	private JPanel colorPanelMain;
	private JComboBox<String> colorList;
	
	class ListenerMgr implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (colorList.getSelectedItem()=="Red"){
				colorPanel.setBackground(new Color(255,0,4));	
				colorPanelMain.setBackground(new Color(255,0,4));
			}
			else if (colorList.getSelectedItem()=="Green"){
				colorPanel.setBackground(new Color(62,255,36));	
				colorPanelMain.setBackground(new Color(62,255,36));
			}
			else if (colorList.getSelectedItem()=="Blue"){
				colorPanel.setBackground(new Color(36,255,245));	
				colorPanelMain.setBackground(new Color(36,255,245));
			}
		}
	}
		
	public GUI_4(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new BorderLayout());
		colorPanel.setBackground(new Color(255,255,255));
		
		colorPanelMain = new JPanel();
		colorPanelMain.setLayout(new FlowLayout());
		colorPanelMain.setBackground(new Color(255,255,255));
		
		colorList = new JComboBox<String>();
		colorList.addItem("Red");
		colorList.addItem("Green");
		colorList.addItem("Blue");	
		
		colorList.addActionListener(new ListenerMgr());
		
		colorPanelMain.add(colorList);
		colorPanel.add(colorPanelMain, BorderLayout.SOUTH);
		frame.add(colorPanel, BorderLayout.CENTER);	
		
		frame.setVisible(true);
	}
}
