package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class GUI_5 {
	private JFrame frame;
	private JPanel colorPanel;
	private JPanel colorPanelMain;
	private JMenuBar menuBar;
	private JMenu colorList;
	private JMenuItem red;
	private JMenuItem green;
	private JMenuItem blue;
		
	public GUI_5(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new BorderLayout());
		colorPanel.setBackground(new Color(255,255,255));
		
		colorPanelMain = new JPanel();
		colorPanelMain.setLayout(new FlowLayout());
		colorPanelMain.setBackground(new Color(255,255,255));
		
		menuBar = new JMenuBar();
		
		colorList = new JMenu("Color");
		red = new JMenuItem("Red");
		green = new JMenuItem("Green");
		blue = new JMenuItem("Blue");
	
		colorList.add(red);
		colorList.add(green);
		colorList.add(blue);	
		
		red.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				colorPanel.setBackground(new Color(255,0,4));	
				colorPanelMain.setBackground(new Color(255,0,4));
			}
		});
		
		green.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				colorPanel.setBackground(new Color(62,255,36));	
				colorPanelMain.setBackground(new Color(62,255,36));
			}
		});
		
		blue.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				colorPanel.setBackground(new Color(36,255,245));	
				colorPanelMain.setBackground(new Color(36,255,245));
			}
		});
		
		menuBar.add(colorList);
		colorPanelMain.add(menuBar);
		colorPanel.add(colorPanelMain, BorderLayout.NORTH);
		frame.setJMenuBar(menuBar);
		frame.add(colorPanel, BorderLayout.CENTER);	
		
		frame.setVisible(true);
	}
}
