package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Control.Controller;

public class GUI_7 {
	private int key = 0;
	private Controller control;
	private JFrame frame;
	private JPanel panelMain;
	private JPanel panel;
	private JPanel panel2;
	private JMenuBar menuBar;
	private JMenu option;
	private JMenuItem deposit;
	private JMenuItem withdraw;
	private JLabel title;
	private JTextField money;
	private JButton submit;
	private JTextArea balance;
		
	public GUI_7(){
		control = new Controller();
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(530, 240);
		
		panelMain = new JPanel();
		panelMain.setLayout(new FlowLayout());
		
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		
		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		
		
		menuBar = new JMenuBar();
		
		option = new JMenu("Option");
		deposit = new JMenuItem("Deposit");
		withdraw = new JMenuItem("Withdraw");
	
		option.add(deposit);
		option.add(withdraw);
		
		deposit.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				key = 0;
				title.setText(" ฝากจำนวนเงิน : ");
				submit.setText("ฝาก");
			}
		});
		
		withdraw.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				key = 1;
				title.setText(" ถอนจำนวนเงิน : ");
				submit.setText("ถอน");
			}
		});
		
		title = new JLabel(" ฝากจำนวนเงิน : ");		
		
		money = new JTextField(9);
		
		submit = new JButton("ฝาก");
		
		submit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (key == 0){
					control.deposit(money.getText());
					balance.append("จำนวนเงินฝาก :  " + money.getText() + "    บาท\n");
					balance.append("ยอดคงเหลือ :  " + control.getBalance() + "    บาท\n");

				}
				else{
					control.withdraw(money.getText());
					if (control.getValue() == 1){
						balance.append("ไม่สามารถถอนเงินได้\n");
					}
					else{						
						balance.append("จำนวนเงินถอน :  " + money.getText() + "    บาท\n");
						balance.append("ยอดคงเหลือ :  " + control.getBalance() + "    บาท\n");
					}
				}
			}
		});
		
		
		balance = new JTextArea(8,15);
		
		menuBar.add(option);
		panel.add(menuBar);
		panel.add(title);
		panel.add(money);
		panel.add(submit);
		panel2.add(balance);
		panelMain.add(panel);
		frame.add(panelMain);
		frame.add(panel2, BorderLayout.SOUTH);
		frame.setJMenuBar(menuBar);
		frame.setVisible(true);
	}
}
